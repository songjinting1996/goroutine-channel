/*
 * @Author: your name
 * @Date: 2021-04-06 15:10:19
 * @LastEditTime: 2021-04-06 16:43:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /songjintinglinux/go/src/golang_pratice/goroutine_channel/main.go
 */
package main

import (
	"fmt"
	"sync"
)

func getWorker(waitCh chan int, symbol int, wg *sync.WaitGroup) (next chan int) {
	notify := make(chan int)

	//wg.Add(delta int)用来设置工作协程的个数(本质上是把计数器加一)
	wg.Add(1)

	go func(waitCh chan int) {
		//工作协程结束后调用wg.Done()(本质上是把计数器减一)
		defer func() {
			wg.Done()
		}()

		//每个工作协程的执行逻辑
		for d := range waitCh {
			if d >= 10 {
				break
			}
			fmt.Println("goroutine:", symbol, "print", d+1)
			notify <- d + 1
		}

		close(notify)
		fmt.Println("goroutine: finish", symbol)
	}(waitCh)

	return notify
}

func main() {
	//sync.WaitGroup类型的对象有三个方法:Add(),Done(),Wait()
	wg := new(sync.WaitGroup)

	start := make(chan int)

	//循环创建goroutine,每次创建的goroutine的waitCh channel为上次创建的goroutine返回的notify channel
	lastCh := start
	for i := 0; i < 3; i++ {
		lastCh = getWorker(lastCh, i+1, wg)
	}

	//将最后一个lastCh channel的内容读取到start channel中，这样，就构成了一个环
	start <- 0
	for v := range lastCh {
		start <- v
	}

	close(start)

	//等待所有goroutine都执行结束后返回(本质上是等待计数器的值为0)
	wg.Wait()
}
